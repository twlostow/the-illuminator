EESchema Schematic File Version 5
EELAYER 33 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 5676 4475
Wire Wire Line
	5026 3325 5026 3675
Wire Wire Line
	5026 3975 5026 4475
Wire Wire Line
	5026 4475 5676 4475
Wire Wire Line
	5676 4475 5676 4775
Wire Wire Line
	5676 4475 6426 4475
Wire Wire Line
	5726 3325 5026 3325
Wire Wire Line
	6126 3325 6426 3325
Wire Wire Line
	6426 3325 6426 3475
Wire Wire Line
	6426 3775 6426 3975
Wire Wire Line
	6426 4275 6426 4475
Wire Notes Line width 0
	7100 5250 7100 6250
Wire Notes Line width 0
	7100 6250 7150 6250
Wire Notes Line width 0
	7150 6250 10450 6250
Wire Notes Line width 0
	10450 5250 7100 5250
Wire Notes Line width 0
	10450 6250 10450 5250
Text Notes 7200 6150 0    50   ~ 0
Copyright Sam Smith 2020.\n\nThis source describes Open Hardware and is licensed under the CERN-OHL-S v2.\n\nYou may redistribute and modify this documentation and make products\nusing it under the terms of the CERN-OHL-S v2 (https:/cern.ch/cern-ohl).\nThis documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED\nWARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY\nAND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-S v2\nfor applicable conditions.
Text Notes 8500 6850 2    118  ~ 24
The Illuminator
Text Notes 8686 7640 2    50   ~ 0
15 April 2020
Text Notes 8900 7500 2    50   ~ 0
The Illumniator - main circuit diagram
Text Notes 9800 7000 2    50   ~ 0
A trivial project to demonstrate licensing hardware under CERN OHL.
Text Notes 10700 7640 2    50   ~ 0
0.1
$Comp
L power:GND #PWR01
U 1 1 5EAF3FEF
P 5676 4775
F 0 "#PWR01" H 5676 4525 50  0001 C CNN
F 1 "GND" H 5681 4602 50  0000 C CNN
F 2 "" H 5676 4775 50  0001 C CNN
F 3 "" H 5676 4775 50  0001 C CNN
	1    5676 4775
	1    0    0    -1  
$EndComp
$Comp
L device:R R1
U 1 1 5EAF2D3C
P 6426 3625
F 0 "R1" H 6496 3670 50  0000 L CNN
F 1 "100" H 6496 3580 50  0000 L CNN
F 2 "" V 6356 3625 50  0001 C CNN
F 3 "" H 6426 3625 50  0001 C CNN
	1    6426 3625
	1    0    0    -1  
$EndComp
$Comp
L device:LED D1
U 1 1 5EAF28CF
P 6426 4125
F 0 "D1" V 6464 4007 50  0000 R CNN
F 1 "Red, Green or Orange LED" V 6373 4007 50  0000 R CNN
F 2 "" H 6426 4125 50  0001 C CNN
F 3 "" H 6426 4125 50  0001 C CNN
	1    6426 4125
	0    -1   -1   0   
$EndComp
$Comp
L device:Battery_Cell BT1
U 1 1 5EAF32C3
P 5026 3875
F 0 "BT1" H 5144 3970 50  0000 L CNN
F 1 "Lithium Cell" H 5144 3880 50  0000 L CNN
F 2 "" V 5026 3935 50  0001 C CNN
F 3 "" V 5026 3935 50  0001 C CNN
	1    5026 3875
	1    0    0    -1  
$EndComp
$Comp
L switches:SW_Push SW1
U 1 1 5EAF30B5
P 5926 3325
F 0 "SW1" H 5926 3609 50  0000 C CNN
F 1 "Pushbutton" H 5926 3518 50  0000 C CNN
F 2 "" H 5926 3525 50  0001 C CNN
F 3 "" H 5926 3525 50  0001 C CNN
	1    5926 3325
	1    0    0    -1  
$EndComp
$EndSCHEMATC
